Conditions aren't just for Commerce anymore!

Using this module you can define conditions that can be:
- added via a user-friendly widget to an entity
- given AND/OR or negation rules
- checked programatically for pass/failure
- extended via plugin definitions

USE CASES
---------

Election positions where the user who can vote for a position may need to have a certain user role, for instance.

Whether someone can join a Group is determined by whether they have purchased a specific product.

FEATURES
--------

Features:

- Allows multiple conditions fields on an entity
- combined into groups which are themselves checked for pass/failure

HOW TO USE
----------

How to create your own condition plugins:

TBD

How to evaluate a conditions field in your custom module:

TBD

---

- Create a widget in src/Plugin/Field/FieldWidget that extends one of the existing widgets, with the field_types including conditions_plugin_item:{your plugin type ID} - you do not need to do anything other than extend if the widget is fine

To use in a Base Field Definition, use conditions_plugin_item:{your plugin type ID}


CREDITS
-------

Inspired by and based on https://www.drupal.org/project/commerce_conditions_plus
