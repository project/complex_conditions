<?php

namespace Drupal\complex_conditions;

use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ComplexConditionInterface;
use Drupal\complex_conditions\Plugin\Field\FieldType\PluginItemInterface;

/**
 * [Description ComplexConditionsEntityTrait]
 */
trait ComplexConditionsEntityTrait
{
  /**
   * {@inheritdoc}
   */
  public function getConditionsForField($field_name)
  {
    $conditions = [];
    $index = 0;
    foreach ($this->get($field_name) as $field_item) {
      /** @var PluginItemInterface $field_item */
      $condition = $field_item->getTargetInstance();
      $id = $condition->getPluginDefinition()['id'] . $index;
      $conditions[$id] = $condition;
      $index++;
    }
    return $conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function setConditionsForField(array $conditions, $field_name)
  {
    $this->set($field_name, []);
    foreach ($conditions as $condition) {
      if ($condition instanceof ComplexConditionInterface) {
        $this->get($field_name)->appendItem([
          'target_plugin_id' => $condition->getPluginId(),
          'target_plugin_configuration' => $condition->getConfiguration(),
        ]);
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionOperator($conditions_field_name)
  {
    return $this->get($conditions_field_name . '_operator')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setConditionOperator($condition_operator, $conditions_field_name)
  {
    $this->set($conditions_field_name . '_operator', $condition_operator);
    return $this;
  }
}
