<?php

namespace Drupal\complex_conditions;

class ConditionsRenderer {

  public function requirementsTable(array $requirements) {
    $rows = [];
    $groupingsDone = [];

    foreach ($requirements as $requirement) {
      $groupingId = $requirement->getGroupId();
      $grouped = $groupingId && $groupingId != 'ungrouped';
      if ($grouped) {
        $groupingName = $requirement->getGroup() && $requirement->getGroup() != 'ungrouped' ? $requirement->getGroup() : '';
        if (!in_array($groupingId, array_keys($groupingsDone))) {
          $row = [];
          $row['Requirement'] = [
            'data' => [
              '#markup' => '<b>' . $groupingName . '</b>',
            ],
            'class' => [],
          ];
          $row['Pass'] = '';
          $rows[$groupingId] = $row;
          $groupingsDone[$groupingId] = [];
        }
        $groupingsDone[$groupingId][] = $requirement;
      }

      $row = [
        'data' => [],
        'class' => [$grouped ? 'complex-condition__grouping-subcondition' : ''],
      ];

      $row['data']['Requirement'] = [
        'data' => [
          '#markup' => $requirement->getLabel(),
          '#suffix' => $requirement->getDescription() ? '<div class="form-item__description complex-condition__eligibility-description">' . $requirement->getDescription() . '</div>' : '',
        ],
        'class' => [$grouped ? 'complex-condition__grouping-subcondition__label' : ''],
      ];

      $row['data']['Pass'] = [
        'data' => [
          '#markup' => $requirement->getEmoji(),
        ],
        'class' => [$grouped ? 'complex-condition__grouping-subcondition__pass' : ''],
      ];

      $rows[] = $row;
    }

    foreach ($groupingsDone as $groupingId => $requirements) {
      $rows[$groupingId]['Pass'] = ConditionRequirement::getEmojiForState(ConditionRequirement::anyPassed($requirements) ? 'pass' : 'fail');
    }

    $headers = [];
    $headers[] = [
      'data' => t('Requirement'),
    ];
    $headers[] = t('Pass');

    return [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $headers,
    ];
  }

}
