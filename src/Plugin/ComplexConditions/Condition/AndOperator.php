<?php

namespace Drupal\complex_conditions\Plugin\ComplexConditions\Condition;

use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ConditionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides the weight condition for shipments.
 *
 * @ComplexCondition(
 *   id = "complex_conditions_and_operator",
 *   label = @Translation("And group (all conditions in group true)"),
 *   category = @Translation("Conditions"),
 * )
 */
final class AndOperator extends OperatorBase
{

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    $config = parent::defaultConfiguration();
    $config['group_label'] = t('All of these conditions must be met');
    return $config;
  }

  /**
   * @inheritDoc
   */
  public function evaluate(EntityInterface $entity, AccountInterface $account, $parameters = [])
  {
    // @todo find child via config? evaluate there?
    return TRUE;
  }
}
