<?php

namespace Drupal\complex_conditions\Plugin\ComplexConditions\Condition;

use Drupal\complex_conditions\Plugin\ComplexConditions\Condition\ConditionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

abstract class OperatorBase extends ComplexConditionBase
{

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [
        'group_label' => [],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['group_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Condition group label'),
      '#default_value' => $this->configuration['group_label'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['group_label'] = $values['group_label'];
  }


  /**
   * @inheritDoc
   */
  public function evaluate(EntityInterface $entity, AccountInterface $account, $parameters = [])
  {
    // @todo find child via config? evaluate there?
    return TRUE;
  }
}
